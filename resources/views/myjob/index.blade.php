@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">My Jobs
                <div class="float-right">
                    <a href="{{url('myjobs/create')}}" class="btn btn-primary">
                        Create New
                    </a>
                </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Publish Date</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        @foreach($jobs as $job)
                        <tr>
                            <td>{{$job->title}}</td>
                            <td>{{$job->desc}}</td>
                            <td>
                            @switch($job->status)
                                @case(1)
                                    Saved
                                    @break
                                @case(2)
                                    Publish
                                    @break
                            @endswitch
                            </td>
                            <td>{{$job->created_at}}</td>
                            <td>{{$job->publish_date}}</td>
                            <td>
                            @switch($job->status)
                                @case(1)
                                    <a href="{{url('myjobs/edit/'.$job->id)}}" class="btn btn-primary">Edit</a>
                                    @break
                                @case(2)
                                    <a href="{{url('myjobs/view/'.$job->id)}}" class="btn btn-success">View</a>
                                    @break
                            @endswitch
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {!! $jobs->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
