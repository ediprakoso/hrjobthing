@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">View Jobs</div>

                <div class="card-body">
                    <form class="form-horizontal" method="post" action="{{url('myjobs/edit/'.$job->id)}}"> 
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="status" id="status">
                        <div class="row mb-1">
                            <label class="col-lg-3 form-label">Title</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="title" value="{{$job->title}}"/>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label class="col-lg-3 form-label">Description</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name="description" rows="3">
                                    {{$job->desc}}
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-danger" onclick="window.history.back();return false;">Cancel</button>
                                @if($job->status=='1')
                                    <button class="btn btn-primary" onclick="save()">Save</button>
                                    <button class="btn btn-success" onclick="publish()">Publish</button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function save() {
        document.getElementById('status').value='1';    
    }

    function publish() {
        document.getElementById('status').value='2';
    }
</script>
@endsection
