@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">View Jobs</div>

                <div class="card-body">
                    <form class="form-horizontal" method="post" action="{{url('myjobs/edit/'.$job->id)}}"> 
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="status" id="status">
                        <div class="row mb-2">
                            <div class="col-lg-12">
                                <button class="btn btn-info" onclick="window.history.back();return false;">Back</button>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label class="col-lg-3 form-label">Title</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="title" value="{{$job->title}}" readonly/>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label class="col-lg-3 form-label">Description</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name="description" rows="3" readonly>
                                    {{$job->desc}}
                                </textarea>
                            </div>
                        </div>
                    </form>

                    @if($job->status=='2')
                        @if(count($job_applied))
                        <table class="table table-striped">
                            <thead> 
                                <tr>
                                    <th>Freelancer</th>
                                    <th>Proposal</th>
                                    <th>Applied Date</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($job_applied as $item)
                                <tr>
                                    <td>{{$item->UserFreelancer->name}}</td>
                                    <td>{{$item->proposal}}</td>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                    @isset($job->freelancer_id)
                                        @if($job->freelancer_id==$item->freelancer_id)
                                            Choosen
                                        @endif   
                                    @else
                                        <a href="{{url('myjobs/choose/'.$job->id.'/'.$item->freelancer_id)}}" class="btn btn-primary">Choose</a></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        @else
                            <div class="row">
                                <div class="col-lg-12 text-center font-weight-bold">
                                    There is no freelance applied this job yet
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function save() {
        document.getElementById('status').value='1';    
    }

    function publish() {
        document.getElementById('status').value='2';
    }
</script>
@endsection
