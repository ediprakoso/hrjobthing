@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jobs</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Employer</th>
                            <th>Created Date</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        @foreach($jobs as $job)
                        <tr>
                            <td>{{$job->title}}</td>
                            <td>{{$job->desc}}</td>
                            <td>{{$job->UserEmployer->name}}</td>
                            <td>{{$job->created_at}}</td>
                            <td>
                            @if($job->JobsApplied->where('freelancer_id',Illuminate\Support\Facades\Auth::user()->id)->count()>0)
                                <button class="btn btn-success">Applied</button>
                            @else
                                <a href="{{url('jobs/applied/'.$job->id)}}" class="btn btn-primary">Apply</a></td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                    {!! $jobs->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
