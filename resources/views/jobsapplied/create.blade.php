@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jobs Applied</div>

                <div class="card-body">
                    <form class="form-horizontal" method="post" action="{{url('jobs/applied')}}"> 
                        @csrf
                        <input type="hidden" name="jobs_id" value="{{$job->id}}"/>
                        <div class="row">
                            <label class="col-lg-3 form-label">Title</label>
                            <span class="col-lg-9">{{$job->title}}</span>
                        </div>
                        <div class="row mb-1">
                            <label class="col-lg-3 form-label">Description</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="3" readonly>
                                    {{trim($job->desc)}}
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="col-lg-3 form-label">Proposal</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="3" name="proposal">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-danger" onclick="window.history.back();return false;">Cancel</button>
                                @isset($usersType)
                                    @if($profile->point_left>0)
                                        <button class="btn btn-primary">Apply</button>
                                    @else
                                        <span>You have no point left</span>
                                    @endif
                                @else
                                    <button class="btn btn-primary">Apply</button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
