@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jobs you Applied</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Employer</th>
                            <th>Job Date</th>
                            <th>Applied Date</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        @foreach($jobs_applied as $job)
                        <tr>
                            <td>{{$job->Jobs->title}}</td>
                            <td>{{$job->Jobs->UserEmployer->name}}</td>
                            <td>{{$job->Jobs->publish_date}}</td>
                            <td>{{$job->created_at}}</td>
                            <td>
                            @isset($job->Jobs->freelancer_id)
                                @if($job->Jobs->freelancer_id==$job->freelancer_id)
                                    Choosen
                                @endif
                            @else
                                <a href="{{url('jobs/applied/del/'.$job->id)}}" class="btn btn-danger">Cancel</a></td>
                            @endisset
                        </tr>
                        @endforeach
                    </table>
                    {!! $jobs_applied->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
