<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobsApplied extends Model
{
    protected $table='jobs_applied';

    public function scopeUserJobsApplied($query,$id,$paginate=true)
    {
        if ($paginate) {
            return $query->where('freelancer_id',$id)->paginate(10);
        } else {
            return $query->where('freelancer_id',$id);
        }
    }

    public function UserFreelancer()
    {
        return $this->belongsTo('App\User', 'freelancer_id');
    }

    public function Jobs()
    {
        return $this->belongsTo('App\Models\Job', 'jobs_id');
    }
}
