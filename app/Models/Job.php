<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table='jobs';

    public function scopeUserJobs($query,$id)
    {
        return $query->where('employer_id',$id);
    }

    public function scopeUserJobsOpen($query,$id)
    {
        return $query->where('employer_id',$id)->whereNull('freelancer_id');
    }

    public function scopeJobsOpen($query,$paginate=true)
    {
        // if ($paginate) {
            // return $query->whereNull('freelancer_id')->paginate(10);
        // } else {
            return $query->whereNull('freelancer_id');
        // }
    }

    public function scopeUserJobsClosed($query,$id)
    {
        return $query->where('employer_id',$id)->whereNotNull('freelancer_id');
    }

    public function JobsApplied()
    {
        return $this->hasMany('App\Models\JobsApplied', 'jobs_id');
    }

    public function UserEmployer()
    {
        return $this->belongsTo('App\User', 'employer_id');
    }
}
