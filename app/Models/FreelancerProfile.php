<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreelancerProfile extends Model
{
    protected $table='freelancer_profile';

    public function Rank()
    {
        return $this->belongsTo('App\Models\Rank', 'rank');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'freelancer_id');
    }
}
