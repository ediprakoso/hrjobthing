<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Job;
use App\Models\JobsApplied;

class MyJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs=Job::UserJobs(Auth::user()->id)
            ->with('JobsApplied')
            ->paginate(10);
        return view('myjob.index',compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('myjob.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $job=new Job;
            $job->title=$request->input('title');
            $job->desc=$request->input('description');
            $job->status=$request->input('status');
            $job->employer_id=Auth::user()->id;
            if($request->input('status')=='2'){
                $job->publish_date=date("Y-m-d");
            }
            $job->save();
            return redirect('myjobs')->with('message','Success');
        } catch (\Exception $e) {
            return redirect('myjobs')->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job=Job::where('id',$id)
            ->first();
        $job_applied=JobsApplied::where('jobs_id',$id)
            ->with('UserFreelancer')
            ->get();
        // dd($job);
        return view('myjob.show',compact('job','job_applied'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job=Job::where('id',$id)
            ->with('JobsApplied.UserFreelancer')
            ->first();
        // dd($job);
        return view('myjob.edit',compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $job=Job::find($id);
            $job->title=$request->input('title');
            $job->desc=$request->input('description');
            $job->status=$request->input('status');
            $job->employer_id=Auth::user()->id;
            if($request->input('status')=='2'){
                $job->publish_date=date("Y-m-d");
            }
            $job->save();
            return redirect('myjobs')->with('message','Success');
        } catch (\Exception $e) {
            return redirect('myjobs')->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function choose($id,$freelance_id)
    {
        try {
            $job=Job::find($id);
            $job->freelancer_id=$freelance_id;
            $job->save();
            return redirect('myjobs')->with('message','Success Choose');
        } catch (\Exception $e) {
            return redirect('myjobs')->with('error',$e->getMessage());
        }
    }
}
