<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\JobsApplied;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jobs=Job::JobsOpen();
        $jobs_applied=JobsApplied::UserJobsApplied(1)->with('Jobs')->get();
        // dd($jobs);
        return view('home',compact('jobs','jobs_applied'));
    }
}
