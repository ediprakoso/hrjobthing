<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\JobsApplied;
use App\Models\FreelancerProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JobsAppliedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs_applied=JobsApplied::where('freelancer_id',Auth::user()->id)
            ->with('Jobs.UserEmployer')
            ->paginate(10);
        // dd($jobs_applied);
        return view('jobsapplied.index',compact('jobs_applied'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $job=Job::find($id);
        $profile=FreelancerProfile::where('freelancer_id',Auth::user()->id)->first();
        return view('jobsapplied.create',compact('job','profile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $job=new JobsApplied;
            $job->proposal=$request->input('proposal');
            $job->jobs_id=$request->input('jobs_id');
            $job->freelancer_id=Auth::user()->id;
            $job->save();
            $profile=FreelancerProfile::where('freelancer_id',Auth::user()->id)->first();
            if(!$profile){
                $profil=new FreelancerProfile;
                $profil->freelancer_id=Auth::user()->id;
                $profil->rank='B';
                $profil->point_left=20-2;
                $profil->save();
            } else {
                $profil->point_left-=2;
                $profil->save();
            }
            DB::commit();
            return redirect('jobs')->with('message', 'Success');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('jobs')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JobsApplied::destroy($id);
        $profile=FreelancerProfile::where('freelancer_id',Auth::user()->id)->first();
        if(!$profile){
            $profil=new FreelancerProfile;
            $profil->freelancer_id=Auth::user()->id;
            $profil->rank='B';
            $profil->point_left=20;
            $profil->save();
        } else {
            $profil->point_left+=2;
            $profil->save();
        }
        return redirect('jobs')->with('message', 'Success Cancel');
    }
}
