<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){
    Route::get('/jobs','JobsController@index');
    Route::get('/myjobs','MyJobController@index');
    Route::get('/myjobs/create','MyJobController@create');
    Route::get('/jobs/applied/{id}','JobsAppliedController@create');
    Route::get('/jobs/applied','JobsAppliedController@index');
    Route::get('/jobs/applied/del/{id}','JobsAppliedController@destroy');
    Route::post('/jobs/applied','JobsAppliedController@store');
    Route::post('/myjobs','MyJobController@store');
    Route::put('/myjobs/edit/{id}','MyJobController@update');
    Route::get('/myjobs/edit/{id}','MyJobController@edit');
    Route::get('/myjobs/view/{id}','MyJobController@show');
    Route::get('/myjobs/choose/{id}/{fid}','MyJobController@choose');
});