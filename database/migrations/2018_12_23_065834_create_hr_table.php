<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('desc');
            $table->integer('employer_id');
            $table->integer('status');
            $table->integer('freelancer_id')->nullable();
            $table->date('publish_date')->nullable();
            $table->timestamps();
        });

        Schema::create('jobs_applied',function(Blueprint $table){
            $table->increments('id');
            $table->integer('jobs_id');
            $table->integer('freelancer_id');
            $table->longText('proposal');
            $table->timestamps();
        });

        Schema::create('freelancer_profile',function(Blueprint $table){
            $table->increments('id');
            $table->integer('freelancer_id')->unique();
            $table->string('rank');
            $table->integer('point_left');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('jobs_applied');
        Schema::dropIfExists('freelancer_profile');
    }
}
